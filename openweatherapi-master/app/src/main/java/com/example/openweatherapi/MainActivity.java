package com.example.openweatherapi;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    public static String BaseUrl = "https://api.openweathermap.org/";
    public static String AppId =  "80efebb3a3cd2b46f1c5fd568b6b7fca";
    public static String lat = "7.56";
    public static String lon = "99.62";

    private TextView weatherData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        weatherData = findViewById(R.id.textView);
        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCurrentData();
            }
        });
    }
    void getCurrentData() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        WeatherAPI service = retrofit.create(WeatherAPI.class);
        Call<WeatherResponse> call = service.getCurrentWeatherData(lat, lon,
                AppId);
        call.enqueue(new Callback<WeatherResponse>() {
            @Override
            public void onResponse(@NonNull Call<WeatherResponse> call,
                                   @NonNull Response<WeatherResponse> response) {
                if (response.code() == 200) {
                    WeatherResponse weatherResponse = response.body();
                    assert weatherResponse != null;
                    String stringBuilder = "Country: " +
                            weatherResponse.getSys().getCountry() +
                            "\n" +
                            "Name: " +
                            weatherResponse.getName() +
                            "\n" +
                            "Temperature: " +
                            weatherResponse.getMain().getTemp() +
                            "\n" +
                            "Temperature(Min): " +
                            weatherResponse.getMain().getTempMin() +
                            "\n" +
                            "Temperature(Max): " +
                            weatherResponse.getMain().getTempMax() +
                            "\n" +
                            "Humidity: " +
                            weatherResponse.getMain().getHumidity() +
                            "\n" +
                            "Pressure: " +
                            weatherResponse.getMain().getPressure();
                    weatherData.setText(stringBuilder);
                }
            }
            @Override
            public void onFailure(@NonNull Call<WeatherResponse> call,
                                  @NonNull Throwable t) {
                weatherData.setText(t.getMessage());
            }
        });
    }
}